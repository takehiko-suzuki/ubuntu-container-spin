# Ubuntu with SSL (Password login)
Ubuntu container with SSL.

# How to set password
Specify `ROOT_PASSWORD` in `.env` file.
```shell:.env
ROOT_PASSWORD=<password of root>
```
